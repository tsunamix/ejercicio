package ejercicio;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hotelbeds.supplierintegrations.hackertest.detector.Message;

public class MainTest {

	@Test
	public void com_hotelbeds_supplierintegrations_hackertest_detector_message(){
		
		String sString="";
		
		sString = null;
		assertEquals(null
				,Message.getInstance(sString));
		
		sString = "";
		assertEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.10.75,1336129471,SIGNING_SUCCESS,Will.Smith";
		
		assertNotEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.1110.75,1336129471,SIGNING_SUCCESS,Will.Smith";
		assertEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.10.75,1336129471,SIGNIDG_SUCCESS,Will.Smith";
		assertEquals(null
				,Message.getInstance(sString));
		
	}

}
