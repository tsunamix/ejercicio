package com.hotelbeds.supplierintegrations.hackertest.detector;

/**
* 
* Enum with possible actions from logs lines
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public enum Action {
	SIGNING_SUCCESS,
	SIGNING_FAILURE
}

