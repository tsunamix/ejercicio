package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.util.Hashtable;

import com.hotelbeds.utils.Tools;

/**
* 
* This is my facade class that implements the HackerDetector interface. This class add all 
* the logical layer for look for laste IP access and save it at memory
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public class HackerDetectorEngine implements HackerDetector{

	/**
	 * 
	 * Hashtable where HackDetectorEngine manage all the access, it use a hash IP for easy 
	 * access.
	 * 
	**/
	
	private Hashtable<Long, LogsStack> hash = new Hashtable(); 
	
	/**
	 * Check a log line and apply all the logical behabiuor for determinate if is valid or not
	 * 
	 * @param line line of log.
	 * @return NULL if is valid or String if the IP address attacker.
	**/
	
	public String parseLine(String line) {
		
		//Parse the log line to a Message class, I love object�oriented!!
		Message m = Message.getInstance(line);
		
		//Check if it valid and if its action is failure
		if (m != null && m.getAction() == Action.SIGNING_FAILURE){
			
			//Create a hash with a long number, more faster than a string in our hastable
			long hashIp = Tools.ipToLong(m.getIP());
			
			//Get actual hashtable value
			Object o = hash.get(hashIp);
			
			LogsStack stack = null;
			
			//It's your first time or not?
			if (o == null)
				stack = new LogsStack();
			else
				stack = (LogsStack)o;
				
			//Add the message to LogsStack
			stack.add(m);
			
			//And save to the hashtable again
			hash.put(hashIp, stack);
			
			//Check if there is a invalid access in logStack for that IP address
			if ( !stack.isValid() )
				return m.getIP();
			
		}
		
		return null;
	}
}
