package com.hotelbeds.sandbox;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.hotelbeds.supplierintegrations.hackertest.detector.HackerDetectorEngine;

/**
* 
* This is my main class. I simulate all the test behaviors. Thanks LogsFactory I am going 
* in inject into the HackerDetectEngine some logs lines. 
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/


public class MainHackerDetector {

	public static final Logger logger = Logger.getLogger(MainHackerDetector.class);
	
	public static void main(String[] args) {
		//Start setting log, for audit all event in the system
		//PropertyConfigurator.configure(ClassLoader.getSystemResource("log4j.properties").getFile());
		
		//Start the singleton facade the implements our interface HackerDetector
		HackerDetectorEngine hde = new HackerDetectorEngine();
		
		
		
		List<String> logs = new ArrayList<String>();
		
		logs.add("10.162.10.75,1380779573,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779583,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779593,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779673,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779683,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779693,SIGNING_FAILURE,Will.Smith");
		logs.add("10.162.10.75,1380779699,SIGNING_FAILURE,Will.Smith");
		
		
		
		//Retrive all fake 
		for (String log : logs)  
		{
			logger.info(String.format("Check new log : '%s'", log));
			
			//Sending log to our system, check if it is valid or nor
			String sIp=hde.parseLine(log);
			
			if (sIp != null){
				logger.info( String.format("Threat has been detected from %s",sIp) );
			}
			else{
				logger.info("No threat, is OK");
			}
		}
		
	}

}
